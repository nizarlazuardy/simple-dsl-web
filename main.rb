#!/usr/bin/env ruby

require 'sinatra'
require 'dotenv'

Dotenv.load

stage = ENV["environment"]
ruby_version = RUBY_VERSION

get '/' do
    if status 200
        content_type :json
        { 
            message: 'Hello world!',
            description: 'hello world by ruby lang.'
        }.to_json
    else
        p 'API missmatch'
    end
end

get '/version' do
    if status 200
        content_type :json
        { 
            version: ruby_version,
            environment: stage
        }.to_json
    else
        p 'API missmatch'
    end
end
