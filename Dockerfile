FROM ruby:2.7.8-alpine

ARG STAGE

WORKDIR /usr/src/app
RUN adduser --disabled-password --gecos "" ruby
RUN apk update && apk --no-cache add --virtual build-dependencies build-base

COPY Gemfile ./
RUN bundle install

COPY . .
RUN echo "$STAGE" >> .env && source .env
RUN chown -R ruby:ruby /usr/src/app
RUN chmod +x main.rb
USER ruby
EXPOSE 9292
CMD ["bundle", "exec", "puma", "-p", "9292"]
