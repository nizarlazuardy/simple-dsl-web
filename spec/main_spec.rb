require_relative '../main'
require 'test/unit'
require 'rack/test'

class MyHelloWorld < Test::Unit::TestCase
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  def test_root_endpoint
    get '/'
    assert last_response.ok?
  end

  def test_version_endpoint
    get '/version'
    assert last_response.ok?
  end
end